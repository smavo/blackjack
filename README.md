BLACK JACK V2.0.0

![image](/uploads/a39d17ef1958bd309407fe6dc91d43a9/image.png)
![image](/uploads/0eb4c109ccd26a22ae3a90eb036cd496/image.png)
![image](/uploads/271e5746629a031f8032e64b155455a3/image.png)
![image](/uploads/92b9824e8bfd41be45df8606bf2dccbe/image.png)

##  Creator of the BLACK JACK project
👤 **Sergio V.O**
* Twitter: https://twitter.com/smavodev
* Instagram: https://www.instagram.com/smavodev
* Github: [@smavo](https://github.com/smavo)
* Gitlab: [@smavo] (https://gitlab.com/smavo)
* LinkedIn: https://www.linkedin.com/in/smavodev